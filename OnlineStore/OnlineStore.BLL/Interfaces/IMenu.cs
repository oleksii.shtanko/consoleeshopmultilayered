﻿using OnlineStore.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.BLL.Interfaces
{
    public interface IMenu
    {
        public string Role();
        public string ChooseOptions();
        public bool IsActive { get; set; }
    }
}
