﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.BLL.DTO
{
    public  class UserDTO
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Type { get; set; }
    }
}
