﻿using OnlineStore.BLL.DTO;
using OnlineStore.BLL.Interfaces;
using OnlineStore.DAL.Entities;
using OnlineStore.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.BLL.Services
{
    public class AdminService: IMenu
    {
        public UserDTO CurrentUser;
        UserRepository userRepository = new UserRepository();
        ProductRepository productRepository = new ProductRepository();
        OrderRepository orderRepository = new OrderRepository();
        public AdminService(UserDTO user)
        {
            CurrentUser = user;
        }
        public bool IsActive { get; set; } = true;
        public delegate void LogOutHendler();
        public event LogOutHendler NotifyOfLoggingOut;
        private List<ProductQuantity> ProductList = new List<ProductQuantity>();
        public string SeeUsersInfo()
        {
            return GetList(userRepository.GetUserList());
        }
        public string AllProducts()
        {
            return GetList(productRepository.GetProductList());
        }
        private string GetList<T>(IEnumerable<T> list)
        {
            string res = "";

            foreach (T item in list)
            {
                res += item.ToString() + "\n";
            }

            return res;
        }
        public string Role()
        {
            return "You are Admin";
        }
        public string ChangeUserPassword()
        {
            Console.WriteLine("User Id:");

            int userId;
            try
            {
                userId = Convert.ToInt32(Console.ReadLine());
            }
            catch (OverflowException)
            {
                return "Please input corrent value";
            }
            User user = userRepository.SearchUser(userId);
            if (user is null)
            {
                return "User not found";
            }
            Console.WriteLine("Enter  new password:");
            var newPassword = Console.ReadLine();
            if (newPassword == "" || newPassword == null)
            {
                return "Password cant be empty";
            }
            userRepository.ChangeUserPassword(user, newPassword);
            return "Success";
        }
        public string AddInBucket()
        {
            Console.WriteLine("Input product name:");
            string name = Console.ReadLine();
            Product prod = productRepository.SearchProductByName(name);

            if (prod is null)
            {
                return "Cant find product with this name.";
            }

            if (ProductList.Any(item => item.product.Name == prod.Name))
            {
                ProductQuantity productInCart = ProductList.FirstOrDefault(item => item.product.Name == prod.Name);
                productInCart.Quantity++;
                return productInCart.ToString();
            }
            else
            {
                var newItem = new ProductQuantity(prod);
                ProductList.Add(newItem);
                return newItem.ToString();
            }
        }
        public string ClearBucket()
        {
            ProductList = new List<ProductQuantity>();
            return "Bucket clearned";
        }
        public string ConfirmOrder()
        {
            if (ProductList.Count == 0)
            {
                return "Your bucket is empty";
            }

            Order order = new Order()
            {
                Items = ProductList,
                UserId = CurrentUser.Id
            };

            orderRepository.AddNewOrder(order);
            ProductList = new List<ProductQuantity>();
            return "Success";
        }
        public string ShowBucket()
        {
            if (ProductList.Count == 0)
                Console.WriteLine("Your bucket is empty");
            return GetList(ProductList);
        }
        public string AddProduct()
        {
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Category:");
            string category = Console.ReadLine();
            Console.WriteLine("Decription:");
            string desc = Console.ReadLine();
            Console.WriteLine("Price:");
            int cost = Convert.ToInt32(Console.ReadLine());
            if (name == "" || name == null)
                throw new ArgumentException("Name cant be empty", nameof(name));
            if (category == "" || category == null)
                throw new ArgumentException("Category cant be empty", nameof(category));
            if (desc == "" || desc == null)
                throw new ArgumentException("Description cant be empty", nameof(desc));
            if (cost <= 0)
                throw new ArgumentException("Price must be >0", nameof(cost));
            var product = new Product { Name = name, Category = category, Description = desc, Cost = cost };
            productRepository.AddNewProduct(product);
            return "Success";
        }


        public string ModifyProduct()
        {
            Console.WriteLine("Input product Id:");
            int productId = Convert.ToInt32(Console.ReadLine());
            Product product = productRepository.SearchProductById(productId); ;
            if (product is null)
            {
                return "Product not found";
            }

            Product productModified = new Product();
            Console.WriteLine("New name:");
            productModified.Name = Console.ReadLine();
            Console.WriteLine("Category:");
            productModified.Category = Console.ReadLine();
            Console.WriteLine("Decription:");
            productModified.Description = Console.ReadLine();
            Console.WriteLine("New price:");
            productModified.Cost = Convert.ToDecimal(Console.ReadLine());
            if (productModified.Name == "")
                throw new ArgumentException("Name cant be empty", nameof(productModified.Name));
            if (productModified.Cost <= 0 )
                throw new ArgumentException("Price must be >0", nameof(productModified.Cost));
            productRepository.ModifyProduct(product, productModified.Name, productModified.Category, productModified.Description, productModified.Cost);
            return "Success";
        }

        public string SeeOrdersOfUser()
        {
            Console.WriteLine("Input user Id:");
            int userId = Convert.ToInt32(Console.ReadLine());

            if (!IsUserExists(userId))
            {
                return "User not found";
            }
            return GetList(orderRepository.GetListOfUserOrders(userId));
        }

        private bool IsUserExists(int userId)
        {
            var user = userRepository.SearchUser(userId);
            if (user == null)
                return true;
            return false;
        }
        public string LogOut()
        {
            NotifyOfLoggingOut?.Invoke();
            return "You logged out";
        }
        public string ChangeOrderStatus()
        {
            Console.WriteLine("Input ID of the order:");
            int orderId = Convert.ToInt32(Console.ReadLine());

            Order order = orderRepository.GetOrderById(orderId);
            if (order is null)
            {
                return "No order with Id";
            }

            Console.WriteLine("Select status you want this order to have(New, Canceled, PaymentReceived, Sent, Received, Complete):");
            string status = "";
            switch (Console.ReadLine().ToLower())
            {
                case "new":
                    status = "New";
                    break;
                case "canceled":
                    status = "CanceledByAdmin";
                    break;
                case "paymentreceived":
                    status = "PaymentReceived";
                    break;
                case "sent":
                    status = "Sent";
                    break;
                case "received":
                    status = "Received";
                    break;
                case "complete":
                    status = "Complete";
                    break;
            }
            Order targetOrder = orderRepository.GetOrderById(orderId);

            if (targetOrder != null)
            {
                if (status != "")
                {
                    targetOrder.OrderStatus = status;
                    return "Success";
                }
                else
                {
                    return "Please enter correct status";
                }
            }
            return "Failed";
        }

        public string FindProdByName()
        {
            Console.WriteLine("Input name:");

            string name = Console.ReadLine().Trim();

            Product prod = productRepository.SearchProductByName(name);

            if (prod != null)
            {
                return prod.ToString();
            }
            else
            {
                return "Product not found";
            }
        }
        public string ChooseOptions()
        {
            PrintOptions();

            return ExecuteOption(GetOption());
        }

        private string GetOption()
        {
            return Console.ReadLine().Trim();
        }

        private string ExecuteOption(string opt)
        {
            Dictionary<string, Func<string>> dict = GetOptionDict();

            try
            {
                return dict[opt]();
            }
            catch (KeyNotFoundException)
            {
                return "Incorrect input!";
            }
        }
        private Dictionary<string, Func<string>> GetOptionDict()
        {
            var options = new Dictionary<string, Func<string>>
            {
                { "1", SeeUsersInfo },
                { "2", ChangeUserPassword },
                { "3", AllProducts },
                { "4", FindProdByName },
                { "5", AddInBucket },
                { "6", ShowBucket },
                { "7", ConfirmOrder },
                { "8", ClearBucket },
                { "9", AddProduct },
                { "10", ModifyProduct },
                { "11", SeeOrdersOfUser },
                { "12", ChangeOrderStatus },
                { "13", LogOut },
            };
            return options;
        }

        private void PrintOptions()
        {
            Console.WriteLine("1| See users info");
            Console.WriteLine("2| Change user's password");
            Console.WriteLine("3| All Products");
            Console.WriteLine("4| Find product by name");
            Console.WriteLine("5| Add in bucket");
            Console.WriteLine("6| Show cart");
            Console.WriteLine("7| Confirm order");
            Console.WriteLine("8| Clear Bucket");
            Console.WriteLine("9| Add product");
            Console.WriteLine("10| Modify product");
            Console.WriteLine("11| See orders of user");
            Console.WriteLine("12| Change order status");
            Console.WriteLine("13| Log Out");
        }
    }
}
