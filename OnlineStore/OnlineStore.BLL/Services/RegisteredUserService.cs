﻿using OnlineStore.BLL.DTO;
using OnlineStore.BLL.Interfaces;
using OnlineStore.DAL.Entities;
using OnlineStore.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.BLL.Services
{
   public class RegisteredUserService : IMenu
    {
        
        UserRepository userRepository = new UserRepository();
        ProductRepository productRepository = new ProductRepository();
        OrderRepository orderRepository = new OrderRepository();
        public UserDTO CurrentUser { get; set; }
        public RegisteredUserService(UserDTO user)
        {
            CurrentUser = user;
        }
        public bool IsActive { get; set; } = true;
        private List<ProductQuantity> ProductList = new List<ProductQuantity>();
        public delegate void LogOutHendler();
        public event LogOutHendler NotifyOfLoggingOut;
        public string AllProducts()
        {
            return GetList(productRepository.GetProductList());
        }
        public string Role()
        {
            return "You are RegisteredUser";
        }
        private protected string GetList<T>(IEnumerable<T> list)
        {
            string res = "";
            foreach (T item in list)
            {
                res += item.ToString() + "\n";
            }
            return res;
        }

        public string FindProdByName()
        {
            Console.WriteLine("Input product name:");
            string name = Console.ReadLine().Trim();
            Product prod = productRepository.SearchProductByName(name);
            if (prod != null)
            {
                return prod.ToString();
            }
            else
            {
                return "Cant find product with this name.";
            }
        }
        public string AddInBucket()
        {
            Console.WriteLine("Input the name of the product:");
            string name = Console.ReadLine();
            Product prod = productRepository.SearchProductByName(name);
            if (prod is null)
            {
                return "There is no such product in the store.";
            }

            if (ProductList.Any(item => item.product.Name == name))
            {
                ProductQuantity productInCart = ProductList.FirstOrDefault(item => item.product.Name == prod.Name);
                productInCart.Quantity++;
                return productInCart.ToString();
            }
            else
            {
                var newItem = new ProductQuantity(prod);
                ProductList.Add(newItem);
                return newItem.ToString();
            }
        }


        public string ShowBucket()
        {
            if (ProductList.Count == 0)
                Console.WriteLine("Your bucket is empty");
            return GetList(ProductList);
        }

        public string ConfirmOrder()
        {
            if (ProductList.Count == 0)
            {
                return "Your bucket is empty";
            }
            Order order = new Order()
            {
                Items = ProductList,
                UserId = CurrentUser.Id
            };
            orderRepository.AddNewOrder(order);
            ProductList = new List<ProductQuantity>();
            return "Success";
        }

        public string ClearBucket()
        {
            ProductList = new List<ProductQuantity>();
            return "Bucket clearned";
        }

        public string SeeOrderHistory()
        {
            return GetList(orderRepository.GetOrderHistory(CurrentUser.Id));
        }

        public string SetStatusReceived()
        {
            Console.WriteLine("Input order Id");
            try
            {
                int Id = Convert.ToInt32(Console.ReadLine());
                if (orderRepository.IsCurrentIdExist(CurrentUser.Id, Id))
                {
                    return "There is no order with Id.";
                }
                orderRepository.SetNewOrderStatus(Id);
                return "Success";
            }
            catch (FormatException)
            {
                return "Id must be number";
            }
        }

        public string CancelOrder()
        {
            Console.WriteLine("Input order Id");
            try
            {
                int Id = Convert.ToInt32(Console.ReadLine());
                if (orderRepository.IsCurrentIdExist(CurrentUser.Id, Id))
                {
                    return "There is no order with with Id.";
                }
                var order = orderRepository.GetOrderById(Id);

                if (order.OrderStatus == "Received")
                {
                    return "You cant cansel order";
                }

                order.OrderStatus = "CanceledByUser";
                return "Success";
            }
            catch (FormatException)
            {
                return "Id must be number";
            }
        }

        public string ChangePassword()
        {
            Console.WriteLine("Input your password:");
            string pass = Console.ReadLine();

            if (pass != CurrentUser.Password)
            {
                return "Wrong password";
            }

            Console.WriteLine("Input your new password:");
            string newPass = Console.ReadLine();

            CurrentUser.Password = newPass;
            return "Success";
        }


        public string LogOut()
        {
            NotifyOfLoggingOut?.Invoke();
            return "You logged out";
        }
        public string ChooseOptions()
        {
            PrintOptions();
            return DoAct(GetOption());
        }

        private string GetOption()
        {
            return Console.ReadLine().Trim();
        }

        private string DoAct(string opt)
        {
            Dictionary<string, Func<string>> options = GetOptionDict();
            try
            {
                return options[opt]();
            }
            catch (KeyNotFoundException)
            {
                return "Please select correct option";
            }
        }
        private Dictionary<string, Func<string>> GetOptionDict()
        {
            var options = new Dictionary<string, Func<string>>
            {
                { "1", AllProducts },
                { "2", FindProdByName },
                { "3", AddInBucket },
                { "4", ShowBucket },
                { "5", ConfirmOrder },
                { "6", ClearBucket },
                { "7", SeeOrderHistory },
                { "8", SetStatusReceived },
                { "9", CancelOrder },
                { "10", ChangePassword },
                { "11", LogOut },
            };

            return options;
        }

        private void PrintOptions()
        {
            Console.WriteLine("1| All Products");
            Console.WriteLine("2| Find product by name");
            Console.WriteLine("3| Add in bucket");
            Console.WriteLine("4| Show bucket");
            Console.WriteLine("5| Confirm order");
            Console.WriteLine("6| Clear Bucket");
            Console.WriteLine("7| See order history");
            Console.WriteLine("8| Set 'Received' status");
            Console.WriteLine("9| Cancel order");
            Console.WriteLine("10| Change password");
            Console.WriteLine("11| Log Out");
        }


    }
}

