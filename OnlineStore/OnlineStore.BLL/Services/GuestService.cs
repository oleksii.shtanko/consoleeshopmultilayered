﻿using OnlineStore.BLL.DTO;
using OnlineStore.BLL.Interfaces;
using OnlineStore.DAL.Entities;
using OnlineStore.DAL.Interfaces;
using OnlineStore.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.BLL.Services
{
    public class GuestService : IMenu
    {
        IUnitOfWork Database;
        UserRepository userRepository = new UserRepository();
        ProductRepository productRepository = new ProductRepository();
        OrderRepository orderRepository = new OrderRepository();
        public GuestService()
        {
          
        }
        public UserDTO CurrentUser { get; set; }
        public bool IsActive { get; set; } = true;
        public delegate void LogInHendler(UserDTO user);
        public event LogInHendler NotifyOfLogginIn;

        public string Role()
        {
            return "You are Guest";
        }
        public string AllProducts()
        {
            return GetList(productRepository.GetProductList());
        }

        public string GetList<T>(IEnumerable<T> list)
        {
            string res = "";

            foreach (T item in list)
            {
                res += item.ToString() + "\n";
            }

            return res;
        }

        public string FindProductByName()
        {
            Console.WriteLine("Input product name:");
            string name = Console.ReadLine().Trim();
            Product prod = productRepository.SearchProductByName(name);
            if (prod != null)
            {
                return prod.ToString();
            }
            else
            {
                return "Cant find product with this name";
            }
        }


        public string LogIn()
        {
            Console.WriteLine("Login:");
            string login = Console.ReadLine();
            Console.WriteLine("Password:");
            string password = Console.ReadLine();
            User auth = userRepository.TryUserLogIn(login, password);
            if (auth != null)
            {
                UserDTO newUser = new UserDTO()
                {
                    Id = auth.Id,
                    Login = auth.Login,
                    Password = auth.Password,
                    FullName = auth.FullName,
                    Type = auth.Type
                };
                NotifyOfLogginIn?.Invoke(newUser);
                return "Success";
            }
            else
            {
                return "Wrong login or password";
            }
        }

        public string Register()
        {
            Console.WriteLine("Login:");
            string login = Console.ReadLine();
            if (!userRepository.IsLoginExist(login))
            {
                return "This login already exist";
            }
            Console.WriteLine("Password:");
            string password = Console.ReadLine();
            Console.WriteLine("Full Name:");
            string fullName = Console.ReadLine();
            if (password == "" || password == null)
                throw new ArgumentException("Password cant be null", nameof(password));
            userRepository.AddNewUser(login, password, fullName);
            UserDTO newUser = new UserDTO()
            {
                Password = password,
                Login = login,
                FullName = fullName,
                Type = "User"
            };
            NotifyOfLogginIn?.Invoke(newUser);
            return "Registered success";
        }
        private string GetOption()
        {
            return Console.ReadLine().Trim();
        }

        public string ChooseOptions()
        {
            PrintOptions();

            return DoAct(GetOption());
        }

        private string DoAct(string opt)
        {
            Dictionary<string, Func<string>> dict = GetOptionDict();

            try
            {
                return dict[opt]();
            }
            catch (KeyNotFoundException)
            {
                return "Incorrect input!";
            }
        }

        private Dictionary<string, Func<string>> GetOptionDict()
        {
            var options = new Dictionary<string, Func<string>>
            {
                { "1", AllProducts },
                { "2", FindProductByName },
                { "3", LogIn },
                { "4", Register },
            };

            return options;
        }


        private void PrintOptions()
        {
            Console.WriteLine("1| All Products");
            Console.WriteLine("2| Find product by name");
            Console.WriteLine("3| Log In");
            Console.WriteLine("4| Register");
        }
    }
}