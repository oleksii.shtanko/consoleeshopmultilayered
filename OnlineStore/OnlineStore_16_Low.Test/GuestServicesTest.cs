using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineStore.DAL.Context;
using OnlineStore.DAL.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace OnlineStore.Tests
{
    [TestClass]
    public class GuestServicesTest
    {
        [TestMethod]
        public void GetProductList_ReturnsCorrectList()
        {
            var expected = ContextData.products;
            ProductRepository menu = new ProductRepository();
            var actual = menu.GetProductList();
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void GetProduct_ReturnesCorrect()
        {
            var expected = ContextData.products[0];
            ProductRepository menu = new ProductRepository();
            var actual = menu.SearchProductByName("Apple");
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void LogIn_WorksCorrect()
        {
            string login = "user1";
            string passwrod = "123";
            var expected = true;
            UserRepository menu = new UserRepository();
            var actual = (menu.TryUserLogIn(login, passwrod) != null) ;
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void IsLoginExists_ReturnsCorrectValue()
        {
            string login = "user1";
            var expected = false;
            UserRepository menu = new UserRepository();
            var actual = menu.IsLoginExist(login);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void AddNewUser_WorksCorrect()
        {
            string login = "test";
            UserRepository menu = new UserRepository();
            menu.AddNewUser("test","test", "test");
            Assert.AreEqual(login, ContextData.users.Last().Login);
        }

    }
}
