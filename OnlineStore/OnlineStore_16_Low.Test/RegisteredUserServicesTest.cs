﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineStore.DAL.Context;
using OnlineStore.DAL.Entities;
using OnlineStore.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.Tests
{
    [TestClass]
    public  class RegisteredUserServicesTest
    {
        [TestMethod]
        public void GetProductList_ReturnsCorrectList()
        {
            var expected = ContextData.products;
            ProductRepository menu = new ProductRepository();
            var actual = menu.GetProductList();
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void SearchProductByName_ReturnsCorrectProduct()
        {
                var expected = ContextData.products[0];
                ProductRepository menu = new ProductRepository();
                var actual = menu.SearchProductByName("Apple");
                Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void AddNewOrder_CreateNewOrder()
        {
            OrderRepository menu = new OrderRepository();
            List<ProductQuantity> ProductList = new List<ProductQuantity>();
            Order actual = new Order()
            {
                Items = ProductList,
                UserId = 1,
                OrderStatus = "New"
            };
            menu.AddNewOrder(actual);
            var expected = ContextData.orders.Last();
            Assert.AreEqual(expected, actual);
        }
  
        [TestMethod]
        public void IsCurrentIdExict_ReturnsCorrect()
        {
            OrderRepository menu = new OrderRepository();
            var actual1 = menu.IsCurrentIdExist(1,1);
            var actual2 = menu.IsCurrentIdExist(1,4);
            Assert.IsTrue(actual1);
            Assert.IsFalse(actual2);
        }
        [TestMethod]
        public void SetNewOrderStatus_WorksCorrect()
        {
            OrderRepository menu = new OrderRepository();
            var expected = "Received";
            menu.SetNewOrderStatus(1);
            var actual = ContextData.orders[0].OrderStatus;
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void GetOrderById_ReturnsCorectOrder()
        {
            OrderRepository menu = new OrderRepository();
            var expected = ContextData.orders[0];
            var actual = menu.GetOrderById(1);
            Assert.AreEqual(expected, actual);
        }
    }
}
