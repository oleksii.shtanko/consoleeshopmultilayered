﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineStore.DAL.Context;
using OnlineStore.DAL.Entities;
using OnlineStore.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.Tests
{
    [TestClass]
    public class AdminServicesTest
    {
        [TestMethod]
        public void GetProductList_ReturnsCorrectList()
        {
            var expected = ContextData.products;
            ProductRepository menu = new ProductRepository();
            var actual = menu.GetProductList();
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void GetUsertList_ReturnsCorrectList()
        {
            var expected = ContextData.users;
            UserRepository menu = new UserRepository();
            var actual = menu.GetUserList();
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void ChangeUserPassword_WorksCorrect()
        {
            var expected = "1234";
            UserRepository menu = new UserRepository();
            menu.ChangeUserPassword(ContextData.users[0], "1234");
            Assert.AreEqual(expected, ContextData.users[0].Password);
        }
        [TestMethod]
        public void SearchUser_ReturnsCorrectUser()
        {
            var expected = ContextData.users[0];
            UserRepository menu = new UserRepository();
            var actual = menu.SearchUser(1);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void SearchProductByName_ReturnsCorrectProduct()
        {
            var expected = ContextData.products[0];
            ProductRepository menu = new ProductRepository();
            var actual = menu.SearchProductByName("Apple");
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void AddNewOrder_CreateNewOrder()
        {
            OrderRepository menu = new OrderRepository();
            List<ProductQuantity> ProductList = new List<ProductQuantity>();
            Order actual = new Order()
            {
                Items = ProductList,
                UserId = 1,
                OrderStatus="New"
            };
            menu.AddNewOrder(actual);
            var expected = ContextData.orders.Last();
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void AddNewProduct_CreateNewProduct()
        {
            ProductRepository menu = new ProductRepository();
            var actual = new Product { Name = "test", Category = "test", Description = "test", Cost = 1M };
            menu.AddNewProduct(actual);
            var expected = ContextData.products.Last();
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void SearchProductById_ReturnsCorrectProduct()
        {
            ProductRepository menu = new ProductRepository();
            var expected = ContextData.products[0];
            var actual = menu.SearchProductById(1);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void ModifyProduct_WorksCorrect()
        {
            ProductRepository menu = new ProductRepository();
            var expected = ContextData.products[3];
            menu.ModifyProduct(expected, "test", "test", "test",1M);
            Assert.AreEqual(expected.Name,"test");
            Assert.AreEqual(expected.Category, "test");
            Assert.AreEqual(expected.Description, "test");
            Assert.AreEqual(expected.Cost, 1M);
        }
        [TestMethod]
        public void SearchOrderById_ReturnsCorrectOrder()
        {
            OrderRepository menu = new OrderRepository();
            var expected = ContextData.orders[0];
            var actual = menu.GetOrderById(1);
            Assert.AreEqual(expected, actual);

        }

    }
}
