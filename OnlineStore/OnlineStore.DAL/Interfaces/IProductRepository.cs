﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.DAL.Interfaces
{
    public interface IProductRepository<Product>
    {
        IEnumerable<Product> GetProductList();
        Product SearchProductByName(string name);
        void AddNewProduct(Product product);
        Product SearchProductById(int id);
        void ModifyProduct(Product product, string name, string category, string description, decimal cost);
    }
}
