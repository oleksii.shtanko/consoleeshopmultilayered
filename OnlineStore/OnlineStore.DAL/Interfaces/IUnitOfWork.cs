﻿using OnlineStore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.DAL.Interfaces
{
    public interface IUnitOfWork 
    {
        IUserRepository<User> Users { get; }
        IProductRepository<Product> Products { get; }
        IOrderRepository<Order> Orders { get; }
    }
}
