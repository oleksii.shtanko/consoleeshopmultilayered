﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.DAL.Interfaces
{
    public interface IOrderRepository<Order>
    {
        void AddNewOrder(Order order);
        List<Order> GetOrderHistory(int id);
        bool IsCurrentIdExist(int UserId, int orderId);
        void SetNewOrderStatus(int id);
        Order GetOrderById(int id);
        List<Order> GetListOfUserOrders(int id);
    }
}
