﻿using OnlineStore.DAL.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.DAL.Interfaces
{
    public interface IUserRepository<User>
    {
        IEnumerable<User> GetUserList();
        User SearchUser(int userId);
        void ChangeUserPassword(User user, string password);
        User TryUserLogIn(string login, string password);
        bool IsLoginExist(string login);
        void AddNewUser(string login, string password, string fullName);

    }
}
