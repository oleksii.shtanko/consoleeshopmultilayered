﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.DAL.Entities
{
    public class ProductQuantity
    {
        public readonly Product product;
        public int Quantity { get; set; } = 1;
        public ProductQuantity(Product product)
        {
            this.product = product;
        }
        public override string ToString()
        {
            return $"Product: '{product.Name}', Quantity: {Quantity}";
        }
    }
}
