﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.DAL.Entities
{
    public class Order
    {
        private static int temp = 1;
        public int Id { get; }
        public int UserId { get; set; }
        public List<ProductQuantity> Items { get; set; }
        public string OrderStatus = "New";

        public decimal TotalPrice
        {
            get
            {
                return Items.Sum(item => item.product.Cost * item.Quantity);
            }
        }
        public Order()
        {
            Id = temp;
            temp++;
        }
        public override string ToString()
        {
            string res = $"Order ID: {Id}" + "\n";
            foreach (ProductQuantity item in Items)
            {
                res += $"{item.product.Name} x{item.Quantity}" + "\n";
            }
            res += $"Total Price: {TotalPrice}" + "\n";
            res += $"Order Status: {OrderStatus}" + "\n";
            return res;
        }
    }
}
