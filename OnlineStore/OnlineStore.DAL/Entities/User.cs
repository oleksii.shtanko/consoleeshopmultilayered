﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.DAL.Entities
{
   public class User
    {
        private static int temp = 1;
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Type { get; set; }
        public User()
        {
            Id = temp;
            temp++;
        }
        public override string ToString()
        {

            return $"Id: {Id}, Login: {Login}, Password: {Password},Full Name: {FullName}, Type: {Type}";
        }
    }
}
