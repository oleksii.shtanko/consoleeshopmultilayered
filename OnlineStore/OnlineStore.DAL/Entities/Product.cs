﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore.DAL.Entities
{
    public class Product
    {
        private static int temp = 1;
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public Product()
        {
            Id = temp;
            temp++;
        }
        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name},Category: {Category}, Decription: {Description}, Сost: {Cost}";
        }
    }
}
