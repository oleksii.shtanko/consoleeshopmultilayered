﻿using OnlineStore.DAL.Context;
using OnlineStore.DAL.Entities;
using OnlineStore.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.DAL.Repositories
{
    public class ProductRepository:IProductRepository<Product>
    {
        public IEnumerable<Product> GetProductList()
        {
            return ContextData.products;
        }
        public Product SearchProductByName(string name)
        {
            return ContextData.products.FirstOrDefault(prod => prod.Name == name); ;
        }
        public void AddNewProduct(Product product)
        {
            ContextData.products.Add(product);
        }
        public Product SearchProductById(int id)
        {
            return ContextData.products.FirstOrDefault(prod => prod.Id == id); ;
        }
        public void ModifyProduct(Product product, string name, string category, string description, decimal cost)
        {
            product.Name = name;
            product.Category = category;
            product.Description = description;
            product.Cost = cost;
        }
    }
}
