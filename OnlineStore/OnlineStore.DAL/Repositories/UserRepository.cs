﻿using OnlineStore.DAL.Context;
using OnlineStore.DAL.Entities;
using OnlineStore.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.DAL.Repositories
{
   public class UserRepository:IUserRepository<User>
    {
        public IEnumerable<User> GetUserList()
        {
            return ContextData.users;
        }
        public User SearchUser(int userId)
        {
            return ContextData.users.FirstOrDefault(user => user.Id == userId);
        }
        public void ChangeUserPassword(User user, string password)
        {
            user.Password = password;
        }
        public User TryUserLogIn(string login, string password)
        {
            return ContextData.users.FirstOrDefault(u => (u.Login == login) && (u.Password == password));
        }
        public bool IsLoginExist(string login)
        {
            var loginslist = ContextData.users.Select(u => u.Login);
            if (loginslist.Contains(login))
            {
                return false;
            }
            return true;
        }
        public void AddNewUser(string login, string password, string fullName)
        {
            User newUser = new User()
            {
                Password = password,
                Login = login,
                FullName = fullName,
                Type = "User"
            };
            ContextData.users.Add(newUser);
        }

    }
}

