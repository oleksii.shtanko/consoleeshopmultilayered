﻿using OnlineStore.DAL.Context;
using OnlineStore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStore.DAL.Repositories
{
   public class OrderRepository
    {

        public void AddNewOrder(Order order)
        {
            ContextData.orders.Add(order);
        }
        public List<Order> GetOrderHistory(int id)
        {
            return ContextData.orders.Where(ord => ord.UserId == id).ToList();
        }
        public bool IsCurrentIdExist(int UserId, int orderId)
        {
            return ContextData.orders.Where(ord => ord.UserId == UserId).ToList().Select(o => o.Id).Contains(orderId);
        }
        public void SetNewOrderStatus(int id)
        {
            ContextData.orders.FirstOrDefault(ord => ord.Id == id).OrderStatus = "Received";
        }
        public Order GetOrderById(int id)
        {
            return ContextData.orders.FirstOrDefault(ord => ord.Id == id);
        }
        public List<Order> GetListOfUserOrders(int id)
        {
            return ContextData.orders.Where(ord => ord.UserId == id).ToList();
        }
    }
}
