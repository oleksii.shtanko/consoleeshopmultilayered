﻿using Microsoft.EntityFrameworkCore;
using OnlineStore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using System.Data.Entity;

namespace OnlineStore.DAL.Context
{
   public class ContextData
    {
        public static List<Product> products = new List<Product>()
        {
            new Product { Name = "Apple",Category="Fruits",Description="Red apple",Cost = 0.99M },
            new Product { Name = "Cherry",Category="Fruits",Description="Red cherry",Cost = 3.99M  },
            new Product { Name = "Carrot",Category="Vegetables",Description="Orange carrot" ,Cost = 1.59M },
            new Product { Name = "Cucumber",Category="Vegetables",Description="Green cucumber" ,Cost = 1.39M }
        };
        public static List<User> users = new List<User>()
        {
            new User { Login = "admin", Password = "admin", FullName="Anton Ivanov",Type = "Admin" },
            new User { Login = "user1", Password = "123",FullName="Anton Petrov",Type="User" },
        };
        public static List<Order> orders = new List<Order>()
        {
            new Order
            {
                UserId = 1,
                Items = new List<ProductQuantity>()
                {
                    new ProductQuantity(products[1]){ Quantity = 1 },
                    new ProductQuantity(products[3]){ Quantity = 1 }
                }
            },
            new Order
            {
                UserId = 2,
                Items = new List<ProductQuantity>()
                {
                   new ProductQuantity(products[2]) { Quantity = 1 }
                }
            }

        };

    }
}
