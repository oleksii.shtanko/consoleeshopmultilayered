﻿using Microsoft.AspNetCore.Mvc;
using OnlineStore.BLL.DTO;
using OnlineStore.BLL.Interfaces;
using OnlineStore.BLL.Services;
using System;
using System.Collections.Generic;

namespace OnlineStore
{
    class Program
    {
     
        static void Main(string[] args)
        {
            StartShop shop = new StartShop();
            shop.Start();

        }
    }
}
