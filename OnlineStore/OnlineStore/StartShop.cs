﻿using OnlineStore.BLL.DTO;
using OnlineStore.BLL.Interfaces;
using OnlineStore.BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStore
{
   public class StartShop
    {
       public IMenu menu = new GuestService();
        public void Start()
        {
            (menu as GuestService).NotifyOfLogginIn += LogIn;
            while (true)
            {
                Console.Clear();
                Console.WriteLine(menu.Role());
                Console.WriteLine(menu.ChooseOptions());
                Console.ReadLine();
            }
        }
        public void LogIn(UserDTO user)
        {
            if (user.Type == "Admin")
            {
                menu = new AdminService(user);
                (menu as AdminService).NotifyOfLoggingOut += LogOut;
            }
            else if (user.Type == "User")
            {
                menu = new RegisteredUserService(user);
                (menu as RegisteredUserService).NotifyOfLoggingOut += LogOut;
            }
            else
            {
                menu = new GuestService();
            }
        }
        public void LogOut()
        {
            menu = new GuestService();
            (menu as GuestService).NotifyOfLogginIn += LogIn;
        }

    }
}
